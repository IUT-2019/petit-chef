<!-- 
Institut Universitaire et Technologique d'Orléans
Année : 2018/2019
 -->
 
# Petit Chef


Le projet **Petit Chef** est une réalisation dite one-page qui a pour but de développer les acquis que nous avons obtenus en développement web côté client avec le JavaScript.


### Etudiants
- ORLAY Arnaud, 2A22
- TISSIER Dorian, 2A22


### Installation
```bash
git clone https://gitlab.com/IUT-2019/petit-chef.git # Récupération du projet
yarn add json-server # installation du serveur
```

### Execusion
```bash
./run
# Page accésible sur http://localhost:3000/
```


### Ressources
- [Javascript](https://www.javascript.com/resources)
- [JSON](https://www.json.org/)
- [Node.js](https://nodejs.org/dist/latest-v10.x/docs/api/)
- [Materialize](https://materializecss.com/)
- [yarn](https://yarnpkg.com/en/docs)
