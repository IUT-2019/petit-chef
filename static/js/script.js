/**************************************\
 * API Petit Chef
 * 
 * IUT Informatique d'Orléans
 * 2018 - 2019
 * ORLAY Arnaud & TISSIER Dorian
\**************************************/



// ========== GET ========== \\
function getRecetteAll(){
    $.ajax({
        url: "http://localhost:3000/recettes/",
        type: "GET",
        dataType: "json",
        success: function(recettes){
            console.log(recettes);
        },
        error: function(request, status, error){}
    });
}


function getRecette(id){
    $.ajax({
        url: "http://localhost:3000/recettes/" + id.toString(),
        type: "GET",
        dataType: "json",
        success: function(recette){
            console.log(recette)
        },
        error: function(request, status, error){
            console.log(request);
            console.log(status);
            console.log(error);
        }
    });
}


function getImage(balise, id){
    if(id != null){
        $.ajax({
            url: "http://localhost:3000/ingredients/" + id.toString(),
            type: "GET",
            dataType: "json",
            success: function(image){
                balise.src = "../" + image.img;
                balise.alt = image.nom;
            },
            error: function(request, status, error){
                console.log(request);
                console.log(status);
                console.log(error);
            }
        });
    }
}


function getRecettesByCategorie(categorie){
    let listecategorie = document.querySelector("#recettes > ul");
    let itemcategorie = document.createElement("li");
    let listerecette = document.createElement("ul");
    listerecette.className = "collection with-header";
    itemcategorie.appendChild(listerecette);
    listecategorie.appendChild(itemcategorie);
    
    // Créatoin du titre de la section correspondant à une catégorie
    let firstitemcategorie = document.createElement("li");
    firstitemcategorie.className = "collection-header red darken-2 white-text";
    firstitemcategorie.id = "categorie-" + categorie;
    let firstitemcategorietitle = document.createElement("h4");
    let firstitemcategorietitletext = document.createTextNode(categorie.toUpperCase());
    firstitemcategorietitle.appendChild(firstitemcategorietitletext);
    firstitemcategorie.appendChild(firstitemcategorietitle);
    listerecette.appendChild(firstitemcategorie);

    $.ajax({
        url: "http://localhost:3000/recettes?categorie=" + categorie,
        type: "GET",
        dataType: "json",
        success: function(recettes){
            chargerRecette(recettes, listerecette);
        },
        error: function(request, status, error){}
    });
}


function getCategories(){
    let menu = document.querySelector("#menu > ul");
    $.ajax({
        url: "http://localhost:3000/recettes/",
        type: "GET",
        dataType: "json",
        success: function(recettes){
            // Création de la liste de catégories
            let categories = new Array();
            recettes.forEach( elem => {
                if( !categories.includes(elem.categorie) ){
                    categories.push(elem.categorie);
                }
            });

            // Insertion des catégories dans le DOM
            categories.forEach( categorie => {
                let item = document.createElement("li");
                let link = document.createElement("a");
                link.href = "#categorie-" + categorie;
                link.className = "collection-item"
                let linktext = document.createTextNode(categorie);
                link.appendChild(linktext);
                item.appendChild(link);
                menu.appendChild(item);

                // Chargemet des recettes de cette catégorie
                getRecettesByCategorie(categorie);
            });
        },
        error: function(request, status, error){}
    });
}

getCategories();


function displaySearchBar(){
    let searchbar = document.getElementById("search-bar");
    if(searchbar.style.display == "none"){
        searchbar.style.display = "block";
        document.querySelector("#search-bar #search").focus();
    }
    else {
        searchbar.style.display = "none";
    }
}


function search(){
    let listecategorie = document.querySelector("#recettes > ul");
    let searchbar = document.querySelector("#search");
    if(search.value = undefined){
        listecategorie.innerHTML = "";
        getCategories();
    }
    else {
        $.ajax({
            url: "http://localhost:3000/recettes?q=" + searchbar.value,
            type: "GET",
            dataType: "json",
            success: function(recettes){
                listecategorie.innerHTML = "";
                misenvaleur = new Array();
                recettes.forEach( e => {
                    misenvaleur.push(
                        JSON.parse(
                            JSON.stringify(e).replace(
                                new RegExp(searchbar.value, "gi"),
                                "<span style='background:yellow;'>" + searchbar.value.toLocaleLowerCase() + "</span>"
                            )
                        )
                    );
                });
                    chargerRecette(misenvaleur, listecategorie);
                        
            },
            error: function(request, status, error){}
        });
    }
    displaySearchBar();
}









function chargerRecette(recettes, parent){
    recettes.forEach( recette => {
        let itemrecette = document.createElement("li")
        let recettecontener = document.createElement("section");
        recettecontener.className = "z-depth-2";
        parent.appendChild(itemrecette);
        itemrecette.appendChild(recettecontener);

        // Création de l'entête de recette
        let entete = document.createElement("header");
        entete.className = "row";
        entete.style.backgroundImage = "url('../" + recette.img + "')";
        recettecontener.appendChild(entete);

        // Ajout du titre de recette
        let titre = document.createElement("h3");
        titre.innerHTML = recette.nom;
        entete.appendChild(titre);

        // Contenu de la recette
        let main = document.createElement("main");
        main.className = "row white";
        recettecontener.appendChild(main);

        // Ajout de la liste des ingrédients
        let listeingredient = document.createElement("ul");
        listeingredient.className = "col l4 collection liste-ingredients grey lighten-5";
        recette.ingredients.forEach( ingredient => {
            let listeingredientitem = document.createElement("li");
            listeingredientitem.className = "collection-item";
            let listeingredientitemquantite = document.createElement("span");
            listeingredientitemquantite.className = "secondary-content red-text";
            listeingredientitemquantite.innerHTML = ingredient.quantité;
            let listeingredientitemimage = document.createElement("img");
            getImage(listeingredientitemimage, ingredient.image);
            listeingredientitem.appendChild(listeingredientitemimage);
            
            let encapsulation = document.createElement("span");
            encapsulation.innerHTML = ingredient.nom;
            listeingredientitem.appendChild(encapsulation);

            listeingredientitem.appendChild(listeingredientitemquantite);
            listeingredient.appendChild(listeingredientitem);
        });
        main.appendChild(listeingredient);

        let div = document.createElement("div");
        div.className = "col l8";
        main.appendChild(div);

        // Les caractéristiques
        let tableaucontener = document.createElement("div");
        tableaucontener.className = "row";
        let tableau = document.createElement("table");
        tableau.className = "centered caracteristiques";
        let tableauheader = document.createElement("tr");
        let tableauheaderpersonnes = document.createElement("th");
        tableauheaderpersonnes.innerHTML = "Personnes";
        let tableauheaderpreparation = document.createElement("th");
        tableauheaderpreparation.innerHTML = "Préparation";
        let tableauheadercuisson = document.createElement("th");
        tableauheadercuisson.innerHTML = "Cuisson";
        tableauheader.appendChild(tableauheaderpersonnes);
        tableauheader.appendChild(tableauheaderpreparation);
        tableauheader.appendChild(tableauheadercuisson);
        tableau.appendChild(tableauheader);
        let tableaucoprs = document.createElement("tbody")
        let tableaucorpsligne = document.createElement("tr");
        let tableaucoprspersonne = document.createElement("td");
        tableaucoprspersonne.innerHTML = recette.personnes.toString();
        let tableaucoprspreparation = document.createElement("td");
        tableaucoprspreparation.innerHTML = recette.preparation.durée.toString() + " " + recette.preparation.mesure;
        let tableaucoprscuisson = document.createElement("td");
        if(recette.cuisson != null){
            tableaucoprscuisson.innerHTML = recette.cuisson.durée.toString() + " " + recette.cuisson.mesure;
        }
        else {
            tableaucoprscuisson.innerHTML = "-";
        }
        tableaucorpsligne.appendChild(tableaucoprspersonne);
        tableaucorpsligne.appendChild(tableaucoprspreparation);
        tableaucorpsligne.appendChild(tableaucoprscuisson);
        tableaucoprs.appendChild(tableaucorpsligne);
        tableau.appendChild(tableaucoprs);
        tableaucontener.appendChild(tableau);
        div.appendChild(tableaucontener);

        // Les étapes de préparation
        let titredesciption = document.createElement("h5");
        titredesciption.className = "row";
        let titredesciptiontext = document.createTextNode("Etapes");
        titredesciption.appendChild(titredesciptiontext);
        div.appendChild(titredesciption);
        let etapes = document.createElement("ol");
        etapes.className ="etapes";
        recette.description.forEach( etape => {
            let etapeitem = document.createElement("li");
            etapeitem.innerHTML = etape.tache;
            etapes.appendChild(etapeitem);
        });
        div.appendChild(etapes);
    });
}